# signedpkg

Signed and encrypted packages for deployment

To create such a package have a look at [this script](/fdik/signedpkg/src/branch/master/test/gen_testdata.sh),
which uses [this Python program](/fdik/signedpkg/src/branch/master/utils/encrypt_distribution_archive.py)
to encrypt.

See also the [format specification](/fdik/signedpkg/src/branch/master/format.md).

## How to use this

Signed packages can be used to transport critical data in a secure way. So they
can be used for provisioning for users.

### Use case: provision for a user

This code fragment shows the idea of how to provision for a user. It case the
user was already provisioned this does nothing. In case an error occurs it
throws an exception with the error message.

```
#include <pEp/unpack.hh>

void provision(std::string url)
{
    pEp::UpdateClient::product p { "provisioning data", url };

    UpdateClient::PublicKey update_key;
    UpdateClient::load_key("update_key.der");

    CryptoPP::ed25519PublicKey deployment_key;
    SignedPackage::LoadPublicKey("deployment_key-pub.der", deployment_key);

    CryptoPP::RSA::PrivateKey provisioning_key;
    SignedPackage::LoadPrivateKey("provisioning_key.der", provisioning_key);

    SignedPackage::provision_user(p, update_key, deployment_key,
            provisioning_key);
}
```

## Keys and their meaning

### Update key

The Update key is the public key, which identifies the p≡p update server. It
guarantees that all downloaded data is coming from authorized source.

It is used for the encryption of the network traffic.

### Deployment key

The Deployment key is the public key, with which the downloaded Signed package
can be checked if it was created by the authorized p≡p factory.

It is used for verification of the Distribution signature. The Deployment key
identifies the p≡p factory.

### Provisioning key

The Provisioning key is the private key, which represents the provisioned
Device.  The Signed package is encrypted using the Provisioning key, making it
impossible for Users of other Devices to uncover the Provisioning data.

The Provisioning key identifies either a single Device or a Device group. It
may identify a Device series in case security is traded for easier management.

A Device series is a bunch of Devices managed under one number before
provisioning – usually bundling similar Devices. A Device group is a group of
personal Devices meant to be used together for one User.  Because after
provisioning a Device group is related to exactly one User the Provisioning key
relates to this single User in case of managing Device groups.

It is more secure to manage Device groups instead of Device series. The option
to manage Device series is only there in case the management process does not
support managing Device groups.

When managing single Devices instead of Device groups p≡p is delivering the
same security properties. There is neither a benefit nor a drawback,
respectively.

