# signedpkg file format

The signedpkg file format defines a deployment archive, which contains these
files in its main directory:

- distribution archive in a file named DIST.A
- distribution key in a file named DIST.KEY
- distribution signature in a file named DIST.SIG

To use a signedpkg the user needs:

- public key part of the deployment key
- private key part of the provisioning key

The signedpkg file format supports all archive file formats, which are
supported by [libarchive](https://github.com/libarchive/libarchive/wiki/LibarchiveFormats).
When unsure use the ZIP archive file format.

## distribution archive

The distribution archive contains a file and folder structure of files being
deployed. In the main directory there are only folders. The folder names
in the main directory mark symbolic installation locations, which are user
dependent.

In the p≡p provisioning there may be up to two such installation locations or
exactly one of them:

- PER_USER_DIRECTORY
- PER_MACHINE_DIRECTORY

All content is placed either in these directories or in subdirectories to these
directories, and is installed into locations relative to these.

The distribution archive is encrypted with the distribution key.

## distribution key

The distribution key is the AES<256> key, with which the distribution archive
is encrypted using GCM<AES>. DIST.KEY contains the distribution key encrypted
with the provisioning key using RSA-OAEP.

## distribution signature

The distribution signature is the ed25519 detached signature of the
distribution archive file DIST.A using the deployment key.

## deployment key

The deployment key is an ed25519 keypair stored in a private key part and a BER
encoded public key part.

The private key is used by the factory. The public key is used by the
deployment target.

## provisioning key

The provisioning key is an RSA keypair stored in a private key part and a BER
encoded public key part.

The private key is used by the deployment target. The public key is used by the
factory.
