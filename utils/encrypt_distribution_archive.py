#!/usr/bin/env python3

import argparse
import os

from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def encrypt_distribution_archive(**kwargs):
    with open(kwargs['provisioning_key'], "rb") as provisioning_key_file:
        provisioning_key = serialization.load_der_public_key(provisioning_key_file.read())

    distribution_key = AESGCM.generate_key(bit_length=256)

    encrypted_distribution_key = provisioning_key.encrypt(distribution_key,
            padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(), label=None))

    with open(kwargs['distribution_key'], 'wb') as distribution_key_file:
        distribution_key_file.write(encrypted_distribution_key)

    with open(kwargs['distribution_archive'], 'rb') as distribution_archive_file:
        distribution_archive = distribution_archive_file.read()

    aesgcm = AESGCM(distribution_key)
    nonce = os.urandom(12)
    encrypted_archive = aesgcm.encrypt(nonce, distribution_archive, None)
    distribution_archive = None

    with open(kwargs['output'], 'wb') as encrypted_archive_file:
        encrypted_archive_file.write(nonce)
        encrypted_archive_file.write(encrypted_archive)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='encrypt distribution archive'
            ' using provisioning key',
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--passphrase', '-p', default=None,
            help='optional passphrase to decrypt deployment key')

    parser.add_argument('--provisioning-key', '-P', default='provisioning_key-pub.der',
            help='file with public key part of provisioning key in DER format')

    parser.add_argument('--distribution-archive', '-a', default='DIST.AD',
            help='unencrypted distribution archive file, usually in ZIP format')

    parser.add_argument('--output', '-o', default='DIST.A',
            help='name of output file for encrypted distribution archive')

    parser.add_argument('--distribution-key', '-d', default='DIST.KEY',
            help='name of output file for distribution key')

    args = parser.parse_args()
    encrypt_distribution_archive(**args.__dict__)
