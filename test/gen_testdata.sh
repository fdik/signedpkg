#!/bin/bash

gen_rsa_keypair()
{
    openssl genrsa -out $1.pem 3072
    openssl pkcs8 -nocrypt -in $1.pem -inform PEM -topk8 -outform DER -out $1.der

    openssl rsa -in $1.pem -pubout -outform DER -out $1-pub.der
}

gen_ed25519_keypair()
{
    openssl genpkey -algorithm ED25519 -outform DER -out $1.der
    openssl pkey -in $1.der -inform DER -pubout -outform DER -out $1-pub.der
}

sign_ed25519()
{
    openssl pkeyutl -in $1 -rawin -sign -inkey $2.der > $3
}

# generate deployment key and provisioning key

gen_ed25519_keypair deployment_key
gen_rsa_keypair provisioning_key

# generate archive with test data

mkdir PER_USER_DIRECTORY
echo content > PER_USER_DIRECTORY/testfile.testdata

zip -r DIST.AD PER_USER_DIRECTORY

# generate distribution key encrypted with provisioning key and encrypt
# distribution archive

../utils/encrypt_distribution_archive.py

# sign archive

sign_ed25519 DIST.A deployment_key DIST.SIG

# gen sample package

zip pEp.ppk DIST.A DIST.KEY DIST.SIG
